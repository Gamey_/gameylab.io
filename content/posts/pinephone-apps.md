+++
author = "Gamey"
title = "PINEPHONE: 5 amazing Linux phone apps!"
date = "2021-04-05"
description = "5 amazing Linux phone apps!"
tags = [
    "Pinephone",  "5 Apps",
]

+++

Today I thought I would make a small list with some of the in my opinion best Pinephone apps. As always it's done on DanctNIX Arch with Phosh and my 2GB Pinephone and since I don't have a Librem5 I can't say anything specific about it but most of the apps should work across all true mobile GNU/Linux Phones.

## Tootle: 
Tootle is a simple GTK Mastodon client written in Vala. It's fully mobile compatible and a generally nice experience since a few updates ago and works perfect for me. It's in fact one of my favorite Pinephone apps by now so I can't recommend it enough!

| ![](https://gateway.pinata.cloud/ipfs/QmcaXEkUtTrs4kVuwRPMPos3LdyKUx52W6UbQC6XkDvC7y/20210404_23h09m41s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmcaXEkUtTrs4kVuwRPMPos3LdyKUx52W6UbQC6XkDvC7y/20210404_23h10m03s_grim.png) |
|-------|-------|
| ![](https://gateway.pinata.cloud/ipfs/QmcaXEkUtTrs4kVuwRPMPos3LdyKUx52W6UbQC6XkDvC7y/20210404_23h10m26s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmcaXEkUtTrs4kVuwRPMPos3LdyKUx52W6UbQC6XkDvC7y/20210404_23h10m48s_grim.png) |

| Type: | Package: |
|-------|-------|
| Source | https://github.com/bleakgrey/tootle |
| AUR | tootle tootle-git |
| Flathub | https://flathub.org/apps/details/com.github.bleakgrey.tootle |
| Mobian | tootle |
| PostmarketOS | tootle |
| ArchARM |  |

## Audiotube:
Audiotube is a Youtube music player made with QML and Kirigami and written in C++. It makes use of the popular youtube-dl youtube downloader and is written mobile first. While Audiotube is very new and not that feature rich yet it already works great and is my recommendation if you want Youtube music on your Pinephone!

| ![](https://gateway.pinata.cloud/ipfs/QmNaYS5nsUmwaRqx4LG4HchNzvMyUf9VgKPDQHRm3HxA5U/20210324_23h03m03s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmNaYS5nsUmwaRqx4LG4HchNzvMyUf9VgKPDQHRm3HxA5U/20210324_23h03m55s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmNaYS5nsUmwaRqx4LG4HchNzvMyUf9VgKPDQHRm3HxA5U/20210324_23h04m48s_grim.png) |
|-------|-------|-------|
| ![](https://gateway.pinata.cloud/ipfs/QmNaYS5nsUmwaRqx4LG4HchNzvMyUf9VgKPDQHRm3HxA5U/20210324_23h05m12s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmNaYS5nsUmwaRqx4LG4HchNzvMyUf9VgKPDQHRm3HxA5U/20210324_23h05m32s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmNaYS5nsUmwaRqx4LG4HchNzvMyUf9VgKPDQHRm3HxA5U/20210324_23h06m23s_grim.png) |

| Type: | Package: |
|-------|-------|
| Source | https://invent.kde.org/jbbgameich/audiotube |
| AUR | audiotube-git |
| Flathub |  |
| Mobian |  |
| PostmarketOS |  |
| ArchARM |  |

## Foliate:
Foliate is a GTK EBook reader and while I still prefer good old Books I did read a bit on my Pinephone to and it was a really nice experience. The issue when switching pages is fixed for a while now so the only problem I had with this amazing app doesn't exist anymore. If you are like me and like to read a little bit on the train and own a GNU/Linux smartphone you should give Foliate a shot!

In case you plan to read a book any time soon I can't recommend "The age of surveillance capitalism" by "Shoshana Zuboff" enough. The title is a bit provocative but the book is simply a masterpiece and a must read for anyone interested in digital privacy :D

| ![](https://gateway.pinata.cloud/ipfs/QmUZU3FnmdBCxweNvbT3QChMbpA3SASTG5bkA4qP281Me7/20210404_19h48m01s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmUZU3FnmdBCxweNvbT3QChMbpA3SASTG5bkA4qP281Me7/20210404_20h02m01s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmUZU3FnmdBCxweNvbT3QChMbpA3SASTG5bkA4qP281Me7/20210404_20h02m31s_grim.png) |
|-------|-------|-------|
| ![](https://gateway.pinata.cloud/ipfs/QmUZU3FnmdBCxweNvbT3QChMbpA3SASTG5bkA4qP281Me7/20210404_20h03m19s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmUZU3FnmdBCxweNvbT3QChMbpA3SASTG5bkA4qP281Me7/20210404_20h03m57s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmUZU3FnmdBCxweNvbT3QChMbpA3SASTG5bkA4qP281Me7/20210404_20h04m52s_grim.png) |

| Type: | Package: |
|-------|-------|
| Source | https://github.com/johnfactotum/foliate |
| AUR |  |
| Flathub |  https://flathub.org/apps/details/com.github.johnfactotum.Foliate |
| Mobian |  |
| PostmarketOS | foliate |
| ArchARM | foliate |

## Portfolio:
Portfolio is a very minimal mobile first file manager made with GTK and written in Python. It's still very new but my by far favorite file manager specially on Phosh and while it still lacks some features the implemented ones work really well. If you are after a more feature rich alternative Index the Plasma mobile file manager is a great choice to.

| ![](https://gateway.pinata.cloud/ipfs/QmfJXiF5G3zymFcVyCr3hQp2cd6usQPtrcVAU5NzhkBxiG/20210404_23h13m54s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmfJXiF5G3zymFcVyCr3hQp2cd6usQPtrcVAU5NzhkBxiG/20210404_23h14m23s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmfJXiF5G3zymFcVyCr3hQp2cd6usQPtrcVAU5NzhkBxiG/20210404_23h14m45s_grim.png) |
|-------|-------|-------|
| ![](https://gateway.pinata.cloud/ipfs/QmfJXiF5G3zymFcVyCr3hQp2cd6usQPtrcVAU5NzhkBxiG/20210404_23h15m03s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmfJXiF5G3zymFcVyCr3hQp2cd6usQPtrcVAU5NzhkBxiG/20210404_23h15m51s_grim.png) |  |

| Type: | Package: |
|-------|-------|
| Source | https://github.com/tchx84/Portfolio |
| AUR | portfolio-file-manager |
| Flathub | https://flathub.org/apps/details/dev.tchx84.Portfolio |
| Mobian |  |
| PostmarketOS | portfolio |
| ArchARM |  |

## Quickddit:
Quickddit is a awesome and feature rich Reddit client for users with and without a account. It's originally made for Ubuntu Touch with QML and C++ but also works perfectly fine on other distros. It's the in my opinion best Reddit client for the Pinephone but I have to admit that I don't use Reddit that much. If you get the chance make sure to try it out at some point :D

| ![](https://gateway.pinata.cloud/ipfs/QmXKSas7gHFnTXkcGYxtAyR7Go4eEtHZSTsaARwDaHyZZ8/20210404_23h18m08s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmXKSas7gHFnTXkcGYxtAyR7Go4eEtHZSTsaARwDaHyZZ8/20210404_23h18m26s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmXKSas7gHFnTXkcGYxtAyR7Go4eEtHZSTsaARwDaHyZZ8/20210404_23h19m09s_grim.png) |
|-------|-------|-------|
| ![](https://gateway.pinata.cloud/ipfs/QmXKSas7gHFnTXkcGYxtAyR7Go4eEtHZSTsaARwDaHyZZ8/20210404_23h19m40s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmXKSas7gHFnTXkcGYxtAyR7Go4eEtHZSTsaARwDaHyZZ8/20210404_23h20m21s_grim.png) | ![](https://gateway.pinata.cloud/ipfs/QmXKSas7gHFnTXkcGYxtAyR7Go4eEtHZSTsaARwDaHyZZ8/20210404_23h20m49s_grim.png) |

| Type: | Package: |
|-------|-------|
| Source | 	https://github.com/accumulator/Quickddit |
| AUR | quickddit |
| Flathub |  |
| Mobian |  |
| PostmarketOS |  |
| ArchARM |  |100.0
